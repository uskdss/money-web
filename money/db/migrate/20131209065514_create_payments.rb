class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :hiduke
      t.integer :kingaku
      t.text :bikou

      t.timestamps
    end
  end
end
